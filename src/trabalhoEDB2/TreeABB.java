package trabalhoEDB2;

public class TreeABB {

	protected Node root;
	protected int size;

	/*M�todo para inserir os elementos da �rvore*/
	public boolean insert(int value) {
		if (root == null) {
			root = new Node(value);
			size = 1;
			return true;
		} else {
			return insert(root, value);
		}
	}
	/********************************************/
	private boolean insert(Node node, int value) {
		if (value > node.value) {
			node.childsRigth += 1;
			if (node.hasRight()) {
				return insert(node.right, value);
			} else {
				node.right = new Node(value);
				size++;
			}
		} else if (value < node.value) {
			node.childsLeft += 1;
			if (node.hasLeft()) {
				return insert(node.left, value);
			} else {
				node.left = new Node(value);
				size++;
			}
		} else {
			return false; 
		}
		return true;
	}

	
	/*Este m�todo vai remover o n� que foi passado por par�metro.*/
	public boolean remove(int value) {
		return remove(root, null, value);
	}
	/*********************************************************/
	private boolean remove(Node node, Node parent, int value) {
		if(node == null) {
			return false;
		} else if(node.value == value) {
			if(node.isLeaf()) {
				updateChild(node, parent, null);
			} else if(node.hasLeft() && !node.hasRight()) {
				updateChild(node, parent, node.left);
			} else if(!node.hasLeft() && node.hasRight()) {
				updateChild(node, parent, node.right);
			} else {
				Node child = node.right;
				if(!child.hasLeft()) {
					child.left = node.left;
					updateChild(node, parent, child);
				} else {
					Node successor = removeSuccessor(child);
					successor.left = node.left;
					successor.right = node.right;
					updateChild(node, parent, successor);
				}
			}
		} else if(value > node.value) {
			return remove(node.right, node, value);
		} else if(value < node.value) {
			return remove(node.left, node, value);
		}
		return true;
	}
	
	
	/*Este m�todo atualiza quem � o pai, o filho e o neto*/
	private void updateChild(Node node, Node parent, Node child) {
		if(parent == null) {
			root = child;
		} else if(node.value > parent.value) {
			parent.right = child;
		} else if(node.value < parent.value) {
			parent.left = child;
		}
	}
	
	
	/*Este m�todo remove do canto e retorna o n� que vai ficar no lugar do que foi removido*/
	protected Node removeSuccessor(Node node) {
		if(!node.left.hasLeft()) {
			Node successor  = node.left;
			node.left = successor.right;
			return successor;
		} else {
			return removeSuccessor(node.left);
		}
	}
	

	/*Este m�todo retorna a altura da �rvore*/    
	public int height() {
		return height(root, 0);
	}
	/****************************************/
	private int height(Node node, int level) {
		if (node == null) { // root
			return level;
		}
		return Math.max(height(node.left, level + 1), height(node.right, level + 1));
	}

	/*Este m�todo retorna o tamanho atual da �rvore*/
	public int size() {
		return size;
	}

	
	/*Este m�todo � chamado quando se tenta retornar um objeto do tipo TreeABB*/
	public String toString() {
		return toString(root);
	}
	/************************************/
	public String toString(Node current) {
		if (current == null) {
			return "";
		}
		String str = toString(current.left);
		
		str += current + "  ";
		str += toString(current.right);
		return str;
	}	
	
	
	/*Este m�todo retorna o elemento que foi passado o �ndice como par�metro*/
	public int enesimoElemento(int index) {
		if(index <= size) {
			return enesimoElemento(root, index, 0, false);
		}
		return -111;
	}
	/******************************************************************************************/
	public int enesimoElemento(Node node, int index, Integer acumulator, boolean isChildRigth) {
		
		if (node != null) {	
			
			int nEsimo1 = enesimoElemento(node.left, index, acumulator, isChildRigth);
			
			if(!isChildRigth) {				
				acumulator = node.childsLeft+1;			// atribuindo valor correto para a vari�vel acumulator, caso o n� n�o seja filho direito de ningu�m.
			}else {
				if(node.hasLeft()) {
					acumulator += node.childsLeft;
				}
				acumulator += 1; 						// atribuindo valor correto para a vari�vel acumulator, caso o n� n�o seja filho direito de algu�m.
			}
			
			if(!isChildRigth) {
				if(index == node.childsLeft+1){
					return node.value;
				}
			}else {
				if(index == acumulator){
					return node.value;
				}
			}
			
			int nEsimo2 = enesimoElemento(node.right, index, acumulator, true);
			
			if(nEsimo1 != -111) {
				return nEsimo1;
			}else {
				return nEsimo2;
			}
			
		}else {
			return -111;
		}
		
	}
	
	
	/*Este m�todo retorna o elemento que est� no �ndice passado por par�metro*/
	public int posicao(int value) {
		
		return posicao(root, value, 0, false);
	}
	/*************************************************************************************/
	protected int posicao(Node node, int value, Integer acumulator, boolean isChildRigth) {
		
		if (node != null) {	
			
			int posicao1 = posicao(node.left, value, acumulator, isChildRigth);
			
			if(!isChildRigth) {				
				acumulator = node.childsLeft+1;			// atribuindo valor correto para a vari�vel acumulator, caso o n� n�o seja filho direito de ningu�m.
			}else {
				if(node.hasLeft()) {
					acumulator += node.childsLeft;
				}
				acumulator += 1; 						// atribuindo valor correto para a vari�vel acumulator, caso o n� n�o seja filho direito de algu�m.
			}
			
			if(value == node.value){
				return acumulator;
			}
			
			int posicao2 = posicao(node.right, value, acumulator, true);
			
			if(posicao1 != -111) {
				return posicao1;
			}else {
				return posicao2;
			}
				
		}else {
			return -111;
		}
		
	}	
	
	
	/*Este m�todo verifica se a �rvore � completa*/
	public boolean ehCompleta() {
		return ehCompleta(root, 0);
	}
	/**************************************************/
	protected boolean ehCompleta(Node node, int index) {
		
		if (node == null) {
			return true;
		} else if (index >= size) {
			return false;
		} else {
			return ehCompleta(node.left, 2*index+1) && ehCompleta(node.right, 2*index+2);
		}
	}
	
	
	/*Este m�todo verifica se a �rvore � cheia*/
	public boolean ehCheia() {
		return ehCheia(root);
	}
	/************************************/
	protected boolean ehCheia(Node node) {
		if (node.left != null && node.right != null) {
			return ehCheia(node.left) && ehCheia(node.right);
		}else if(node.left == null && node.right == null) {
			return true;
		}else {
			return false;
		}
	}
	
	
	/*Este m�todo retorna o elemento que � a mediana da �rvore*/
	public int mediana() {
		return mediana(root, 0, false); 
	}
	/*************************************************************************/
	protected int mediana(Node node, Integer acumulator, boolean isChildRigth) {
		
		if (node != null) {	
			
			int posicao1 = mediana(node.left, acumulator, isChildRigth);
			
			if(!isChildRigth) {				
				acumulator = node.childsLeft+1;			// atribuindo valor correto para a vari�vel acumulator, caso o n� n�o seja filho direito de ningu�m.
			}else {
				if(node.hasLeft()) {
					acumulator += node.childsLeft;
				}
				acumulator += 1; 						// atribuindo valor correto para a vari�vel acumulator, caso o n� n�o seja filho direito de algu�m.
			}
			
			if(size%2 == 0) {
				if(acumulator == (size/2)){
					return node.value;
				}
			}else {
				if(acumulator == (size/2+1)){
					return node.value;
				}
			}
			
			int posicao2 = mediana(node.right, acumulator, true);
			
			if(posicao1 != -111) {
				return posicao1;
			}else {
				return posicao2;
			}	
			
		}else {
			return -111;
		}
	}
	
}
