package trabalhoEDB2;

public class Main {

	public static void main(String[] args) {

TreeABB tree = new TreeABB();
		
		tree.insert(15);
		tree.insert(10);
		tree.insert(8);
		tree.insert(5);
		tree.insert(20);
		tree.insert(25);
		tree.insert(13);
		tree.insert(12);
		tree.insert(14);
		tree.insert(23);
		tree.insert(17);
		tree.insert(16);
		tree.insert(18);
		tree.insert(9);
		//tree.insert(27);   // Descomente para que a �rvore fique cheia
		
		
		System.out.println("-------------------------------------------------------- ");
		System.out.println(tree);
		System.out.println("-------------------------------------------------------- ");
		
		System.out.println("SIZE: " + tree.size());
		
		System.out.println("ENESIMO: " + tree.enesimoElemento(4));
		
		System.out.println("POSI��O: " + tree.posicao(23));
		
		System.out.println("� COMPLETA: " + tree.ehCompleta());
		
		System.out.println("� CHEIA: " + tree.ehCheia());
		
		System.out.println("MEDIANA : " + tree.mediana());
		
		System.out.println("REMOVENDO : " + tree.remove(10));
		
		
		System.out.println("-------------------------------------------------------- ");
		System.out.println(tree);
		System.out.println("-------------------------------------------------------- ");
	

	}

}
