package trabalhoEDB2;

public class Node {

	public int value;
	public int index;
	public int childsLeft;
	public int childsRigth;

	public Node parent;
	public Node left;
	public Node right;
	
	/*Construtor.*/
	public Node(int value) {
		this.value = value;
		this.childsLeft = 0;
		this.childsRigth = 0;
	}
	
	
	/*Construtor.*/
	public Node(int value, Node parent) {
		this.value = value;
		this.parent = parent;
		this.childsLeft = 0;
		this.childsRigth = 0;
	}
	
	
	/*Este m�todo verifica se o n� � uma folha.*/
	public boolean isLeaf() {
		return left == null && right == null;
	}
	
	
	/*Este m�todo verifica se o n� tem filho � esquerda.*/
	public boolean hasLeft() {
		return left != null;
	}

	
	/*Este m�todo verifica se o n� tem filho a direita.*/
	public boolean hasRight() {
		return right != null;
	}

	
	/*M�todo toString do objeto.*/
	public String toString() {
		return Integer.toString(value);
	}
	
}
