# EDB2 - Árvore Binária de Busca

Para a compilação e execução do código, recomendamos o uso da IDE Eclipse. 
Uma vez lá, basta seguir os passos a baixo:

1. clicar em "File" e depois em "Open Projects from File System..."; 
2. selecione "Directory" e selecione a pasta contendo este projeto;
3. clique em "Finish" e pronto. Basta clicar em Run e o programa irá executar. 

Caso esteja usando uma outra IDE e/ou o Terminal da própria máquina, basta: 

1. no terminal vá para o diretório onde os arquivos do tipo .java se encontram;
2. use "javac Exemplo1.java Exemplo2.java" e assim por diante caso hajam mais arquivos, e depois pressione Enter;
3. após compilar use "java Exemplo" para executar, onde Exemplo é o arquivo onde pussui o método main.

É importante ressaltar que é importante ter o JDK instalado em sua máquina e as variáveis de ambiente configuradas devidamente. 
